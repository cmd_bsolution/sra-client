// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueCookie from 'vue-cookie';
import BootstrapVue from 'bootstrap-vue';
import VueLocalStorage from 'vue-localstorage';
import VueResource from 'vue-resource';
import Toasted from 'vue-toasted';
import router from './router';
import store from './store';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap-vue/dist/bootstrap-vue.css';
import '../node_modules/font-awesome/css/font-awesome.min.css';
import App from './App';
import './social';

Vue.use(VueResource);
Vue.use(Toasted, { duration: 3000 });
Vue.use(VueCookie);
Vue.use(BootstrapVue);
Vue.use(VueLocalStorage);
Vue.config.productionTip = false;

// auth interceptors
Vue.http.interceptors.push((request, next) => {
  const token = Vue.cookie.get('token');
  if (token) {
    request.headers.set('Authorization', `Bearer ${token}`);
  }

  next((res) => {
    if (res.status === 401) {
      Vue.cookie.delete('token');
      // check if logout is necessary
      window.location.href = '/?auth';
    } else if (res.status === 403) {
      window.location.href = '/';
    }
  });
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
});
