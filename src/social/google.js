let GoogleAuth;

(function (d, s, id) {
  const fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {
    return;
  }

  const js = d.createElement(s);
  js.id = id;
  js.src = "https://apis.google.com/js/api.js";
  js.onload = function () {
    this.onload = function() {};
    handleClientLoad();
  };

  js.onreadystatechange = function () {
    if (this.readyState === 'complete') {
      this.onload();
    }
  };

  js.defer = true;
  js.async = true;
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'google-api'));

// <script async defer src="https://apis.google.com/js/api.js"
//         onload="this.onload=function(){};handleClientLoad()"
//         onreadystatechange="if (this.readyState === 'complete') this.onload()">
// </script>

function initClient() {
  // Retrieve the discovery document for version 3 of Google Drive API.
  // In practice, your app can retrieve one or more discovery documents.
  const discoveryUrl = 'https://www.googleapis.com/discovery/v1/apis/drive/v3/rest';

  // Initialize the gapi.client object, which app uses to make API requests.
  // Get API key and client ID from API Console.
  // 'scope' field specifies space-delimited list of access scopes.
  gapi.client.init({
    apiKey: process.env.GOOGLE_API_KEY,
    discoveryDocs: [discoveryUrl],
    clientId: process.env.GOOGLE_CLIENT_ID,
    scope: 'email',
  }).then(() => {
    GoogleAuth = gapi.auth2.getAuthInstance();

    // Handle initial sign-in state. (Determine if user is already signed in.)
    const user = GoogleAuth.currentUser.get();
    if (user && user.getAuthResponse && user.getAuthResponse().access_token) {
      window.googleAccessToken = user.getAuthResponse().access_token;
    }
  });
}

function handleClientLoad() {
  // Load the API's client and auth2 modules.
  // Call the initClient function after the modules load.
  gapi.load('client:auth2', initClient);
}
