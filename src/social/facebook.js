window.fbAsyncInit = () => {
  FB.init({
    appId: process.env.FACEBOOK_APP_ID,
    cookie: true,
    xfbml: true,
    version: 'v2.8',
    status: true,
  });

  FB.AppEvents.logPageView();

  FB.getLoginStatus((res) => {
    if (res.authResponse && res.authResponse.accessToken) {
      window.fbAccessToken = res.authResponse.accessToken;
    }
  });
};

// Facebook
(function (d, s, id) {
  const fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {
    return;
  }

  const js = d.createElement(s);
  js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
