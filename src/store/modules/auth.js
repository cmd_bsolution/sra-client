import Vue from 'vue';

const apiData = {
  root: process.env.ENID_URL,
  login: {
    url: '/auth/token/',
    method: 'post',
  },
  signup: {
    url: '/auth/register/',
    method: 'post',
  },
  convertToken: {
    url: '/auth/convert-token/',
    method: 'post',
  },
  userInfo: {
    url: '/auth/me/',
    method: 'get',
  },
  userUpdate: {
    url: '/auth/me/',
    method: 'patch',
  },
  changePassword: {
    url: '/auth/password/',
    method: 'post',
  },
};

function getUserInfo() {
  return new Promise((resolve) => {
    Vue.http[apiData.userInfo.method](`${apiData.root}${apiData.userInfo.url}`).then((res) => {
      if (res && res.data && res.data.id) {
        resolve(res.data);
      } else {
        Vue.cookie.delete('token');
        resolve({});
      }
    }).catch(() => {
      resolve({});
    });
  });
}

const state = {
  currentUser: {},
  loggedIn: false,
};

const getters = {
  isLoggedIn(st) {
    return st.loggedIn;
  },
  getCurrentUser(st) {
    return st.currentUser;
  },
};

const mutations = {
  setUserInfo(st, userInfo) {
    st.currentUser = userInfo;
    st.loggedIn = userInfo && userInfo.id;
  },
  setLoggedIn(st, bool) {
    st.loggedIn = bool;
  },
  logout(st) {
    Vue.cookie.delete('token');
    st.currentUser = {};
    st.loggedIn = false;
  },
};

const actions = {
  initialize({ commit }) {
    return new Promise((resolve, reject) => {
      if (Vue.cookie.get('token')) {
        commit('setLoggedIn', true);
        getUserInfo().then((data) => {
          if (data.id) {
            commit('setUserInfo', data);
          } else {
            commit('setLoggedIn', false);
          }

          resolve(data);
        }).catch((err) => { reject(err); });
      }
    });
  },

  login({ commit }, { email, password }) {
    return new Promise((resolve, reject) => {
      Vue.http[apiData.login.method](`${apiData.root}${apiData.login.url}`, {
        username: email,
        password,
        grant_type: 'password',
        client_id: process.env.CLIENT_ID,
        client_secret: process.env.CLIENT_SECRET,
      }).then((res) => {
        if (res && res.data && res.data.access_token) {
          Vue.cookie.set('token', res.data.access_token, '10h'); // set cookie for 10 hours
          getUserInfo().then((userInfo) => {
            commit('setUserInfo', userInfo);
            resolve(true);
          }).catch(() => resolve(false));
        } else {
          resolve(false);
        }
      }).catch(err => reject(err));
    });
  },

  convertToken({ commit }, { provider, token }) {
    return new Promise((resolve, reject) => {
      Vue.http[apiData.convertToken.method](`${apiData.root}${apiData.convertToken.url}`, { backend: provider, token }).then((res) => {
        if (res && res.data && res.data.access_token) {
          Vue.cookie.set('token', res.data.access_token, '10h'); // set cookie for 10 hours
          getUserInfo().then((userInfo) => {
            commit('setUserInfo', userInfo);
            resolve(true);
          }).catch(() => resolve(false));
        } else {
          resolve(false);
        }
      }).catch(err => reject(err));
    });
  },

  signup({ commit }, { email, password }) {
    return new Promise((resolve, reject) => {
      Vue.http[apiData.signup.method](`${apiData.root}${apiData.signup.url}`, { email, password }).then((res) => {
        if (res && res.status === 201) {
          resolve(actions.login({ commit }, { email, password }));
        } else {
          resolve(false);
        }
      }).catch(err => reject(err));
    });
  },

  updateUserInfo({ commit }, data) {
    return new Promise((resolve, reject) => {
      Vue.http[apiData.userUpdate.method](`${apiData.root}${apiData.userUpdate.url}`, data).then(() => {
        getUserInfo().then((userInfo) => {
          commit('setUserInfo', userInfo);
          resolve(true);
        }).catch(() => resolve(false));
      }).catch(err => reject(err));
    });
  },

  changePassword({ commit }, data) {
    return new Promise((resolve) => {
      Vue.http[apiData.changePassword.method](`${apiData.root}${apiData.changePassword.url}`, data).then(() => {
        resolve(true);
      }).catch(() => resolve(false));
    });
  },

  logout({ commit }) {
    commit('logout');
  },
};

export default {
  state,
  getters,
  mutations,
  actions,
};
