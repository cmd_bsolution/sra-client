import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/Home';
import Sobre from '@/components/Sobre';
import Oportunidades from '@/components/Oportunidades';
import SinglePlace from '@/components/SinglePlace';
import EditProfile from '@/components/EditProfile';
import CreateLocation from '@/components/CreateLocation';
import MyPlaces from '@/components/MyPlaces';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
    },
    {
      path: '/sobre',
      name: 'Sobre',
      component: Sobre,
    },
    {
      path: '/oportunidades',
      name: 'Oportunidades',
      component: Oportunidades,
    },
    {
      path: '/create-location',
      name: 'Create Location',
      component: CreateLocation,
    },
    {
      path: '/my-places',
      name: 'My Places',
      component: MyPlaces,
    },
    {
      path: '/places/:id',
      name: 'Single Place',
      component: SinglePlace,
    },
    {
      path: '/edit-profile',
      name: 'Edit Profile',
      component: EditProfile,
    },
    {
      path: '*',
      redirect: '/', // redirect to home if no previous urls are matched
    },
  ],
});
